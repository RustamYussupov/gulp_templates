module.exports = [
    './gulp/tasks/watch',
    './gulp/tasks/serve',
    './gulp/tasks/scss',
    './gulp/tasks/scripts',
    './gulp/tasks/images',
    './gulp/tasks/images:dev',
    './gulp/tasks/sprite:css',
    './gulp/tasks/sprite:html',
    './gulp/tasks/fonts',
    './gulp/tasks/clean',
    './gulp/tasks/static',
    './gulp/tasks/nunjucks',
];